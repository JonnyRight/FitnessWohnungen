package objects;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.safety.Whitelist;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.vocabulary.VCARD;

public class Triplestore {
	
	private ArrayList<Wohnung> wohnungen = new ArrayList<Wohnung>();
	
	String wohnungsURI = "http://fitnesswohnungen/";

	public Triplestore(ArrayList<Wohnung> wohnungen) {
		super();
		this.wohnungen = wohnungen;
		this.createModel();
	}
	
	
	private void createModel()
	{
		Model model = ModelFactory.createDefaultModel();
		model.setNsPrefix("wohnung", wohnungsURI);
//		Resource wohnungenRdf = model.createResource(wohnungsURI+wohnungen.get(0).getName()).
//				addProperty(ResourceFactory.createProperty(wohnungsURI,"street"),wohnungen.get(0).getStreet()).
//				addProperty(ResourceFactory.createProperty(wohnungsURI,"postal"),wohnungen.get(0).getPostalcode()).
//				addProperty(ResourceFactory.createProperty(wohnungsURI,"latitude"),String.valueOf((wohnungen.get(0).getLatidude()))).
//				addProperty(ResourceFactory.createProperty(wohnungsURI,"longitude"),String.valueOf(wohnungen.get(0).getLongitude()));
				
		for(Wohnung wohnung : wohnungen)
		{
			Resource wohnungenRdf = model.createResource(wohnungsURI+wohnung.getName()).
					addProperty(ResourceFactory.createProperty(wohnungsURI,"street"),wohnung.getStreet()).
					addProperty(ResourceFactory.createProperty(wohnungsURI,"postal"),wohnung.getPostalcode()).
					addProperty(ResourceFactory.createProperty(wohnungsURI,"latitude"),String.valueOf((wohnung.getLatidude()))).
					addProperty(ResourceFactory.createProperty(wohnungsURI,"longitude"),String.valueOf(wohnung.getLongitude()));
			
			addFitnessstudiosToRdf(model, wohnungenRdf, wohnung);
			addSchwimmbadToRdf(model, wohnungenRdf, wohnung);
		}
		
		//model.write(System.out);
		String fileName = "completerdf.rdf";
		FileWriter out = null;
		try {
			out = new FileWriter( fileName );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
		    model.write( out, "RDF/XML-ABBREV" );
		}
		finally {
		   try {
		       out.close();
		   }
		   catch (IOException closeException) {
		       // ignore
		   }
		}
	}
		
	private void addFitnessstudiosToRdf(Model model, Resource wohnungselement, Wohnung wohnung)
	{
		if(!wohnung.getFitnesstudios().isEmpty())
		{
			ArrayList<Fitnessstudio> studios = wohnung.getFitnesstudios();
			for(Fitnessstudio studio: studios)
			{
				model.add(wohnungselement, createProperty("hasFitnessstudio"), model.createResource(wohnungsURI+studio.getName()
		                .replaceAll("\\s+", ""))
		                .addProperty(createProperty("hasStreet"), studio.getStreet())
		                .addProperty(createProperty("hasPostal"), studio.getPostalcode())
		                .addProperty(createProperty("hasLatitude"), String.valueOf(studio.getLatidude()))
		                .addProperty(createProperty("hasLongitude"), String.valueOf(studio.getLongitude())));
			}
		}		
	}
	
	private void addSchwimmbadToRdf(Model model, Resource wohnungselement, Wohnung wohnung)
	{
		if(!wohnung.getFitnesstudios().isEmpty())
		{
			ArrayList<Schwimmbad> schwimmbaeder = wohnung.getSchwimmbaeder();
			for(Schwimmbad bad: schwimmbaeder)
			{
				model.add(wohnungselement, createProperty("hasSchwimmbad"), model.createResource(wohnungsURI+bad.getName()
		                .replaceAll("\\s+", ""))
		                .addProperty(createProperty("hasStreet"), bad.getStreet())
		                .addProperty(createProperty("hasPostal"), bad.getPostalcode())
		                .addProperty(createProperty("hasLatitude"), String.valueOf(bad.getLatidude()))
		                .addProperty(createProperty("hasLongitude"), String.valueOf(bad.getLongitude())));
			}
		}		
	}
	
	private Property createProperty(String relation)
	{
		return ResourceFactory.createProperty(wohnungsURI,relation);
	}
}
