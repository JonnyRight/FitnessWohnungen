package com.rillich.semanticweb;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

public class MyFile {
	
	public void writeTextFile(String textToPrint, String dataName) throws IOException
	{
		String filename = dataName;//e.g. "Astar_log.txt"
		try {
			PrintWriter out = new PrintWriter(new FileWriter(filename, true));
			
			out.println(textToPrint);
			
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Fehler bei der der Erstellung von Textfile");
			e.printStackTrace();
		}
		
	}
	
	public void deletFile(String fileName)
	{
		//file name only
        File file = new File(fileName);
        if(file.delete()){
            System.out.println(fileName+ " File deleted from Project root directory");
        }else System.out.println("File file.txt doesn't exists in project root directory");
	}

}
