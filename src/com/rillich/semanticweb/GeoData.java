package com.rillich.semanticweb;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;


public class GeoData {
	
	public ArrayList<Float> getGeoData(String adress, String postal) throws IOException
	{
		ArrayList<Float> longLat = new ArrayList<Float>();
		HtmlLoader htmlLoader = new HtmlLoader();
		String url = "http://dev.virtualearth.net/REST/v1/Locations/DE/"+postal+"/Leipzig/"+adress+"?key=Aq5L2RQ3Nv5JpJemYaPrglyqIrCBXOMAADtlt5XzkWMLyCvFW793-hIQJmjwzbmQ";
		String bla = proofUrl(adress, postal);
		//System.out.println(url);
		//System.out.println(bla);
		String website = htmlLoader.getCompleteWebsite(bla);
	    
	    int start = website.indexOf("coordinates\":[",0)+14;
	    int end = website.indexOf("]", start);
	    
	    String longLatString = website.substring(start, end);
	    end = longLatString.indexOf(",");
	    String longitude = longLatString.substring(0, end);
	    String latidude = longLatString.substring(end+1);

	    System.out.println(longitude+ " "+ latidude);
		longLat.add(Float.valueOf(longitude));
		longLat.add(Float.valueOf(latidude));

		return longLat;
	}
	
	public String proofUrl(String adress, String postal)
	{
		//String url = "http://dev.virtualearth.net/REST/v1/Locations/DE/"+postal+"/Leipzig/"+adress+"?key=Aq5L2RQ3Nv5JpJemYaPrglyqIrCBXOMAADtlt5XzkWMLyCvFW793-hIQJmjwzbmQ";
		String url2 = "http://dev.virtualearth.net/REST/v1/Locations/DE/"+postal+"/Leipzig/"+adress+"?key=Aq5L2RQ3Nv5JpJemYaPrglyqIrCBXOMAADtlt5XzkWMLyCvFW793-hIQJmjwzbmQ";
		url2 = url2.replace(" ", "");
		//System.out.println(url);
		//System.out.println(url2);
		return url2;
	}
	
	public double calculateDistance(double lat1, double lng1, double lat2, double lng2)
	{
		double earthRadius = 6369; 
		double dLat = Math.toRadians(lat2-lat1);
		double dLng = Math.toRadians(lng2-lng1);
		double a = Math.sin(dLat/2)* Math.sin(dLat/2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))* Math.sin(dLng/2) * Math.sin(dLng/2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double dist = earthRadius * c;
		return dist * 1000; //distance in meters
	}
	

}

//funktioniert
//http://dev.virtualearth.net/REST/v1/Locations/DE/04155/Leipzig/Elsbethstr?key=Aq5L2RQ3Nv5JpJemYaPrglyqIrCBXOMAADtlt5XzkWMLyCvFW793-hIQJmjwzbmQ
